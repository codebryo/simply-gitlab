import resolve from '@rollup/plugin-node-resolve';

export default {
  input: './src/index.js',
  output: {
    file: 'simplify.js',
    format: 'iife',
  },
  plugins: [resolve()],
};
