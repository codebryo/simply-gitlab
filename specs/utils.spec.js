import { please } from '../src/pages/utils';

describe('please.hide', () => {
  it('set the display to none', () => {
    const node = document.createElement('div');
    please.hide(node);

    expect(node.style.display).toBe('none');
  });
});

describe('please.move', () => {
  it('attaches a node to another node', () => {
    const srcNode = document.createElement('div');
    const targetNode = document.createElement('div');

    expect(targetNode.children.length).toBe(0);

    please.move(srcNode, targetNode);

    expect(targetNode.children.length).toBe(1);
  });

  it('can attache multiple nodes to another node', () => {
    const srcNode1 = document.createElement('div');
    const srcNode2 = document.createElement('div');
    const targetNode = document.createElement('div');

    expect(targetNode.children.length).toBe(0);

    please.move([srcNode1, srcNode2], targetNode);

    expect(targetNode.children.length).toBe(2);
  });
});
