import { match } from 'path-to-regexp';
import constants from './constants.js';
import * as Pages from './pages/index.js';

const path = location.pathname;

const key = Object.keys(constants.pages).reduce((acc, route) => {
  const match = match(route, { decode: decodeURIComponent });
  return match(path) ? (acc = route) : acc;
});

const fn = Pages[`${constants.pages[key]}Setup`];
if (typeof fn === 'function') {
  console.log(constants.messages.greeter);
  fn();
}
