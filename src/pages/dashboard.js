import { please, create } from './utils.js';

const NAV_BLOCK = '.nav-block';
const PAGE_TITLE = '#content-body .page-title';

export function setup() {
  const navBlock = document.querySelector(NAV_BLOCK);
  const links = navBlock.querySelectorAll('li');

  const filterBlock = create.block('filter', links, 'ul');

  please.hide(navBlock);
  please.move(filterBlock, document.querySelector(PAGE_TITLE));
}
