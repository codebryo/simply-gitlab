const hide = (node) => (node.style.display = 'none');

const move = (elements, target) => {
  if (!elements.length) return target.appendChild(elements);

  elements.forEach((element) => {
    target.appendChild(element);
  });
};

const getClassName = (str) => `sg__${str}`;

const createBlock = (name, children, type = 'div') => {
  const node = document.createElement(type);
  node.classList.add(getClassName(name));
  move(children, node);
  return node;
};

export const please = {
  hide,
  move,
};

export const create = {
  block: createBlock,
};
