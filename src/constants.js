const greeter = `
*****************************************
* You are using Simply:GitLab Extension *
*****************************************
 If you see odd behaviour, try disabling
  the extension and reloading the page!
-----------------------------------------
    🎉 Brought to you by @codebryo 🎉
`;

export default {
  messages: {
    greeter,
  },
  pages: {
    '/': 'Dashboard',
  },
};
