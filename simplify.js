(function () {
  'use strict';

  const greeter = `
*****************************************
* You are using Simply:GitLab Extension *
*****************************************
 If you see odd behaviour, try disabling
  the extension and reloading the page!
-----------------------------------------
    🎉 Brought to you by @codebryo 🎉
`;

  var constants = {
    messages: {
      greeter,
    },
    pages: {
      '/': 'Dashboard',
    },
  };

  const hide = (node) => (node.style.display = 'none');

  const move = (elements, target) => {
    if (!elements.length) return target.appendChild(elements);

    elements.forEach((element) => {
      target.appendChild(element);
    });
  };

  const getClassName = (str) => `sg__${str}`;

  const createBlock = (name, children, type = 'div') => {
    const node = document.createElement(type);
    node.classList.add(getClassName(name));
    move(children, node);
    return node;
  };

  const please = {
    hide,
    move,
  };

  const create = {
    block: createBlock,
  };

  const NAV_BLOCK = '.nav-block';
  const PAGE_TITLE = '#content-body .page-title';

  function setup() {
    const navBlock = document.querySelector(NAV_BLOCK);
    const links = navBlock.querySelectorAll('li');

    const filterBlock = create.block('filter', links, 'ul');

    please.hide(navBlock);
    please.move(filterBlock, document.querySelector(PAGE_TITLE));
  }

  var Pages = /*#__PURE__*/Object.freeze({
    __proto__: null,
    DashboardSetup: setup
  });

  const path = location.pathname;

  const key = Object.keys(constants.pages).reduce((acc, route) => {
    const match = match(route, { decode: decodeURIComponent });
    return match(path) ? (acc = route) : acc;
  });

  const fn = Pages[`${constants.pages[key]}Setup`];
  if (typeof fn === 'function') {
    console.log(constants.messages.greeter);
    fn();
  }

}());
